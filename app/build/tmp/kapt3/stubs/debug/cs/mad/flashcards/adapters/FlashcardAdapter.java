package cs.mad.flashcards.adapters;

import java.lang.System;

@kotlin.Metadata(mv = {1, 6, 0}, k = 1, d1 = {"\u0000L\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0010 \n\u0002\b\u0002\u0018\u00002\b\u0012\u0004\u0012\u00020\u00020\u0001:\u0001\u001eB\u001d\u0012\u0016\u0010\u0003\u001a\u0012\u0012\u0004\u0012\u00020\u00050\u0004j\b\u0012\u0004\u0012\u00020\u0005`\u0006\u00a2\u0006\u0002\u0010\u0007J\u000e\u0010\u000b\u001a\u00020\f2\u0006\u0010\r\u001a\u00020\u0005J\b\u0010\u000e\u001a\u00020\u000fH\u0016J\u0018\u0010\u0010\u001a\u00020\f2\u0006\u0010\u0011\u001a\u00020\u00022\u0006\u0010\u0012\u001a\u00020\u000fH\u0016J\u0018\u0010\u0013\u001a\u00020\u00022\u0006\u0010\u0014\u001a\u00020\u00152\u0006\u0010\u0016\u001a\u00020\u000fH\u0016J \u0010\u0017\u001a\u00020\f2\u0006\u0010\u0018\u001a\u00020\u00192\u0006\u0010\u001a\u001a\u00020\u00052\u0006\u0010\u0012\u001a\u00020\u000fH\u0002J\u0016\u0010\u001b\u001a\u00020\f2\u000e\u0010\u001c\u001a\n\u0012\u0004\u0012\u00020\u0005\u0018\u00010\u001dR\u001e\u0010\u0003\u001a\u0012\u0012\u0004\u0012\u00020\u00050\u0004j\b\u0012\u0004\u0012\u00020\u0005`\u0006X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0014\u0010\b\u001a\b\u0012\u0004\u0012\u00020\u00050\u0004X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\t\u001a\u00020\nX\u0082.\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u001f"}, d2 = {"Lcs/mad/flashcards/adapters/FlashcardAdapter;", "Landroidx/recyclerview/widget/RecyclerView$Adapter;", "Lcs/mad/flashcards/adapters/FlashcardAdapter$ViewHolder;", "data", "Ljava/util/ArrayList;", "Lcs/mad/flashcards/entities/Flashcard;", "Lkotlin/collections/ArrayList;", "(Ljava/util/ArrayList;)V", "dataSet", "flashDao", "Lcs/mad/flashcards/entities/FlashcardDao;", "addItem", "", "it", "getItemCount", "", "onBindViewHolder", "viewHolder", "position", "onCreateViewHolder", "viewGroup", "Landroid/view/ViewGroup;", "viewType", "showEditDialog", "context", "Landroid/content/Context;", "flashcard", "update", "list", "", "ViewHolder", "app_debug"})
public final class FlashcardAdapter extends androidx.recyclerview.widget.RecyclerView.Adapter<cs.mad.flashcards.adapters.FlashcardAdapter.ViewHolder> {
    private final java.util.ArrayList<cs.mad.flashcards.entities.Flashcard> data = null;
    private final java.util.ArrayList<cs.mad.flashcards.entities.Flashcard> dataSet = null;
    private cs.mad.flashcards.entities.FlashcardDao flashDao;
    
    public FlashcardAdapter(@org.jetbrains.annotations.NotNull()
    java.util.ArrayList<cs.mad.flashcards.entities.Flashcard> data) {
        super();
    }
    
    @org.jetbrains.annotations.NotNull()
    @java.lang.Override()
    public cs.mad.flashcards.adapters.FlashcardAdapter.ViewHolder onCreateViewHolder(@org.jetbrains.annotations.NotNull()
    android.view.ViewGroup viewGroup, int viewType) {
        return null;
    }
    
    @java.lang.Override()
    public void onBindViewHolder(@org.jetbrains.annotations.NotNull()
    cs.mad.flashcards.adapters.FlashcardAdapter.ViewHolder viewHolder, int position) {
    }
    
    private final void showEditDialog(android.content.Context context, cs.mad.flashcards.entities.Flashcard flashcard, int position) {
    }
    
    @java.lang.Override()
    public int getItemCount() {
        return 0;
    }
    
    public final void addItem(@org.jetbrains.annotations.NotNull()
    cs.mad.flashcards.entities.Flashcard it) {
    }
    
    public final void update(@org.jetbrains.annotations.Nullable()
    java.util.List<cs.mad.flashcards.entities.Flashcard> list) {
    }
    
    /**
     * Provide a reference to the type of views that you are using
     * (custom ViewHolder).
     */
    @kotlin.Metadata(mv = {1, 6, 0}, k = 1, d1 = {"\u0000\u001a\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\u0018\u00002\u00020\u0001B\r\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\u0004R\u0011\u0010\u0005\u001a\u00020\u0006\u00a2\u0006\b\n\u0000\u001a\u0004\b\u0007\u0010\b\u00a8\u0006\t"}, d2 = {"Lcs/mad/flashcards/adapters/FlashcardAdapter$ViewHolder;", "Landroidx/recyclerview/widget/RecyclerView$ViewHolder;", "view", "Landroid/view/View;", "(Landroid/view/View;)V", "flashcardTitle", "Landroid/widget/TextView;", "getFlashcardTitle", "()Landroid/widget/TextView;", "app_debug"})
    public static final class ViewHolder extends androidx.recyclerview.widget.RecyclerView.ViewHolder {
        @org.jetbrains.annotations.NotNull()
        private final android.widget.TextView flashcardTitle = null;
        
        public ViewHolder(@org.jetbrains.annotations.NotNull()
        android.view.View view) {
            super(null);
        }
        
        @org.jetbrains.annotations.NotNull()
        public final android.widget.TextView getFlashcardTitle() {
            return null;
        }
    }
}