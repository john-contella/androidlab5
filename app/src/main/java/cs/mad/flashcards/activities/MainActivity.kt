package cs.mad.flashcards.activities

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.room.Room
import cs.mad.flashcards.adapters.FlashcardSetAdapter
import cs.mad.flashcards.databinding.ActivityMainBinding
import cs.mad.flashcards.entities.FlashcardSet
import cs.mad.flashcards.entities.FlashcardSetDao
import cs.mad.flashcards.runOnIO

class MainActivity : AppCompatActivity() {
    private lateinit var binding: ActivityMainBinding
    private lateinit var flashSetDao: FlashcardSetDao

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)


        val db = Room.databaseBuilder(
            applicationContext,
            FlashcardSetDatabase::class.java,
            FlashcardSetDatabase.databaseName
        ).build()

        binding.createSetButton.setOnClickListener {
            var test = FlashcardSet("test")
            (binding.flashcardSetList.adapter as FlashcardSetAdapter).addItem(test)
            runOnIO{flashSetDao.insert(test)}
            binding.flashcardSetList.smoothScrollToPosition((binding.flashcardSetList.adapter as FlashcardSetAdapter).itemCount - 1)

        }
        flashSetDao = db.flashSetDao()


        binding.flashcardSetList.adapter = FlashcardSetAdapter(ArrayList<FlashcardSet>())
        //runOnIO{flashSetDao.deleteAll()}
        loadData()
    }

    private fun loadData() {
        runOnIO { (binding.flashcardSetList.adapter as FlashcardSetAdapter).update(flashSetDao.getAll()) }

    }

}



