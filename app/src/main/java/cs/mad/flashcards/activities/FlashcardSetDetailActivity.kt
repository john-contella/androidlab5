package cs.mad.flashcards.activities

import android.content.Intent
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.room.Room
import cs.mad.flashcards.adapters.FlashcardAdapter
import cs.mad.flashcards.adapters.FlashcardSetAdapter
import cs.mad.flashcards.databinding.ActivityFlashcardSetDetailBinding
import cs.mad.flashcards.entities.Flashcard
import cs.mad.flashcards.entities.FlashcardDao
import cs.mad.flashcards.entities.FlashcardSet
import cs.mad.flashcards.runOnIO

class FlashcardSetDetailActivity : AppCompatActivity() {
    private lateinit var binding: ActivityFlashcardSetDetailBinding
    private lateinit var flashDao: FlashcardDao
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityFlashcardSetDetailBinding.inflate(layoutInflater)
        val view = binding.root
        setContentView(view)


        val db = Room.databaseBuilder(
            applicationContext,
            FlashcardSetDatabase::class.java,
            FlashcardSetDatabase.databaseName
        ).build()

        binding.addFlashcardButton.setOnClickListener {
            var test = Flashcard("test2", "test4")
            (binding.flashcardList.adapter as FlashcardAdapter).addItem(test)
            runOnIO {flashDao.insert(test)}
            binding.flashcardList.smoothScrollToPosition((binding.flashcardList.adapter as FlashcardAdapter).itemCount - 1)

        }

        binding.deleteSetButton.setOnClickListener{
            finish()
        }

        flashDao = db.flashcardDao()

        binding.flashcardList.adapter = FlashcardAdapter(ArrayList<Flashcard>())
        loadData()
        binding.studySetButton.setOnClickListener {
            startActivity(Intent(this, StudySetActivity::class.java))
        }

}

    private fun loadData() {
        runOnIO{(binding.flashcardList.adapter as FlashcardAdapter).update(flashDao.getAll())}
    }
}