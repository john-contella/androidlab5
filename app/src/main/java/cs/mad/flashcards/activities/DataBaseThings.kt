package cs.mad.flashcards.activities

import androidx.room.Database
import androidx.room.RoomDatabase
import cs.mad.flashcards.entities.Flashcard
import cs.mad.flashcards.entities.FlashcardDao
import cs.mad.flashcards.entities.FlashcardSet
import cs.mad.flashcards.entities.FlashcardSetDao



@Database(entities = [FlashcardSet::class, Flashcard::class], version = 1)
abstract class FlashcardSetDatabase: RoomDatabase() {
    companion object {
        const val databaseName = "FLASHCARDSET_DATABASE"
    }
    abstract fun flashSetDao() : FlashcardSetDao
    abstract fun flashcardDao() : FlashcardDao
}
