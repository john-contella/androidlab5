package cs.mad.flashcards.entities
import androidx.room.*

@Entity
data class Flashcard(
    @PrimaryKey var question: String,
    var answer: String
) {
    companion object {
        fun getHardcodedFlashcards(): List<Flashcard> {
            val cards = mutableListOf<Flashcard>()
            for (i in 1..10) {
                cards.add(Flashcard("Term $i", "Def $i"))
            }
            return cards
        }
    }
}



@Dao
interface FlashcardDao {
    @Query("select * from flashcard")
    suspend fun getAll(): List<Flashcard>

    @Insert
    suspend fun insert(flash: Flashcard)

    @Insert
    suspend fun insertAll(flashs: List<Flashcard>)

    @Update
    suspend fun update(flash: Flashcard)

    @Delete
    suspend fun delete(flash: Flashcard)

    @Query("delete from flashcard")
    suspend fun deleteAll()
}