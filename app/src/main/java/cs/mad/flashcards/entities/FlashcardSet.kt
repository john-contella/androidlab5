package cs.mad.flashcards.entities

import androidx.room.*
@Entity
data class FlashcardSet(val title: String, @PrimaryKey val id: Long? = null) {
    companion object {
        fun getHardcodedFlashcardSets(): List<FlashcardSet> {
            val sets = mutableListOf<FlashcardSet>()
            for (i in 1..10) {
                sets.add(FlashcardSet("Set $i"))
            }
            return sets
        }
    }
}


@Dao
interface FlashcardSetDao {
    @Query("select * from flashcardset")
    suspend fun getAll(): List<FlashcardSet>

    @Insert
    suspend fun insert(set: FlashcardSet)

    @Insert
    suspend fun insertAll(sets: List<FlashcardSet>)

    @Update
    suspend fun update(set: FlashcardSet)

    @Delete
    suspend fun delete(set: FlashcardSet)

    @Query("delete from flashcardset")
    suspend fun deleteAll()
}
